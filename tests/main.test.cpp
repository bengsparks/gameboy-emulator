#include <gtest/gtest.h>
#include <iostream>

#include "endianness.hpp"

int main(int argc, char* argv[])
{
	const char* endianness = is_big_endian() ? "BIG ENDIAN" : "LITTLE ENDIAN\n";
	std::cerr << "TESTING FOR " << endianness;

	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
