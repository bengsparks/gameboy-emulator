#include <cstring>
#include <gtest/gtest.h>

#include "common.hpp"

extern "C" {
#include "../src/adc.h"
#include "../src/opcodes.h"
}

static GB_UBYTE
ADC_R8DecodeAndExecute(gameboy* const, GB_UBYTE, GB_UBYTE, GB_UBYTE);

// ADC_R8 = 0x88 to 0x8F inclusive without 0x8E
static std::vector<GB_UBYTE> const ADC_R8_opcodes =
	GenerateOpcodesFromRange(0x88, 0x8F, 0x8E);

// ADD_16 = 0x09, 0x19, 0x29, 0x39
static std::vector<GB_UBYTE> const ADD_R16_opcodes =
	std::vector<GB_UBYTE>{0x09, 0x19, 0x29, 0x39};

TEST(ADC_R8, NoFlags)
{
	InstructionLoop(
		ADC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			ADC_R8DecodeAndExecute(gb, opcode, 1, 5);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) { EXPECT_EQ(flags, 0); });
}

TEST(ADC_R8, ZeroFlag)
{
	InstructionLoop(
		ADC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			ADC_R8DecodeAndExecute(gb, opcode, 0, 0);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) {
			EXPECT_EQ(flags, ZeroMask());
		});
}

TEST(ADC_R8, CarryFlag)
{
	InstructionLoop(
		ADC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			ADC_R8DecodeAndExecute(gb, opcode, 0b10001000, 0b10001000);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) {
			// Zero Mask cannot be set
			EXPECT_EQ(flags & ZeroMask(), 0);
			// Subtract Mask is never set
			EXPECT_EQ(flags & SubtractMask(), 0);
			// Carry Flags should be set
			EXPECT_EQ(flags, CarryMask() | HalfCarryMask());
		});
}

TEST(ADC_R8, AllValues)
{
	InstructionLoop(
		ADC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE opcode) {
			GB_UBYTE upper = std::numeric_limits<GB_UBYTE>::max();
			for (GB_UBYTE i = 0; i < upper; ++i) {
				for (GB_UBYTE j = 0; j < upper; ++j) {
					ResetGameboy(gb);
					ADC_R8DecodeAndExecute(gb, opcode, i, j);
				}
			}
		},
		NoopFlagCheck);
}

static GB_UBYTE ADC_R8DecodeAndExecute(gameboy* const gb,
									   GB_UBYTE instr,
									   GB_UBYTE aValue,
									   GB_UBYTE regValue)
{
	GB_UBYTE* load = nullptr;
	switch (instr) {
		case 0x88: {
			load = B(gb);
			break;
		}
		case 0x89: {
			load = C(gb);
			break;
		}
		case 0x8A: {
			load = D(gb);
			break;
		}
		case 0x8B: {
			load = E(gb);
			break;
		}
		case 0x8C: {
			load = H(gb);
			break;
		}
		case 0x8D: {
			load = L(gb);
			break;
		}
		case 0x8F: {
			load = A(gb);
			break;
		}
	}

	if (instr != 0x8F) {
		SetValueInR8(load, regValue);
	}
	SetValueInR8(A(gb), aValue);

	DecodeAndExecuteInstruction(gb, instr);

	// check for A adding to itself
	if (instr != 0x8F) {
		EXPECT_EQ(*A(gb), static_cast<GB_UBYTE>(aValue + regValue));
	}
	else {
		EXPECT_EQ(*A(gb), static_cast<GB_UBYTE>(2 * aValue));
	}
	return *F(gb);
}
