#include <gtest/gtest.h>

#include "common.hpp"

extern "C" {
#include "../src/gameboy.h"
#include "../src/opcodes.h"
#include "../src/registers.h"
#include "../src/sbc.h"
}

static GB_UBYTE SBC_R8DecodeAndExecute(gameboy*, GB_UBYTE, GB_UBYTE, GB_UBYTE);

// SBC_R8 = 0x98 to 0x9F inclusive without 0x9E
static std::vector<GB_UBYTE> SBC_R8_opcodes =
	GenerateOpcodesFromRange(0x98, 0x9F, 0x9E);

TEST(SBC_R8, SubtractFlag)
{
	InstructionLoop(
		SBC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			SBC_R8DecodeAndExecute(gb, opcode, 0b11, 0b1);
		},
		[](GB_UBYTE const flags, GB_UBYTE const opcode) {
			GB_UBYTE const conditionalZero =
				(opcode == 0x9F ? 1 : 0) * ZeroMask();
			EXPECT_EQ(flags, SubtractMask() | conditionalZero);
		});
}

TEST(SBC_R8, ZeroFlag)
{
	InstructionLoop(
		SBC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			SBC_R8DecodeAndExecute(gb, opcode, 100, 100);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) {
			EXPECT_EQ(flags, SubtractMask() | ZeroMask());
		});
}

TEST(SBC_R8, CarryFlags)
{
	InstructionLoop(
		SBC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			SBC_R8DecodeAndExecute(gb, opcode, 0b00010100, 0b00001100);
		},
		[](GB_UBYTE const flags, GB_UBYTE const opcode) {
			GB_UBYTE const zeroOrCarry =
				(opcode != 0x9F ? HalfCarryMask() : ZeroMask());
			EXPECT_EQ(flags, SubtractMask() | zeroOrCarry);
		});
}

TEST(SBC_R8, AllValues)
{
	InstructionLoop(
		SBC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			GB_UBYTE upper = std::numeric_limits<GB_UBYTE>::max();
			for (GB_UBYTE i = 0; i < upper; ++i) {
				for (GB_UBYTE j = 0; j < upper; ++j) {
					ResetGameboy(gb);
					SBC_R8DecodeAndExecute(gb, opcode, i, j);
				}
			}
		},
		NoopFlagCheck);
}

static GB_UBYTE SBC_R8DecodeAndExecute(gameboy* gb,
									   GB_UBYTE instr,
									   GB_UBYTE aValue,
									   GB_UBYTE regValue)
{
	GB_UBYTE* load = NULL;
	switch (instr) {
		case 0x98: {
			load = B(gb);
			break;
		}
		case 0x99: {
			load = C(gb);
			break;
		}
		case 0x9A: {
			load = D(gb);
			break;
		}
		case 0x9B: {
			load = E(gb);
			break;
		}
		case 0x9C: {
			load = H(gb);
			break;
		}
		case 0x9D: {
			load = L(gb);
			break;
		}
		case 0x9F: {
			load = A(gb);
			break;
		}
	}

	if (instr != 0x9F) {
		SetValueInR8(load, regValue);
	}
	SetValueInR8(A(gb), aValue);

	DecodeAndExecuteInstruction(gb, instr);

	// check for A subtracting from itself
	if (instr != 0x9F) {
		EXPECT_EQ(*A(gb), static_cast<GB_UBYTE>(aValue - regValue));
	}
	else {
		EXPECT_EQ(*A(gb), 0);
	}
	return *F(gb);
}
