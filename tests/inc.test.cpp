#include <gtest/gtest.h>
#include <limits>
#include <vector>

#include "common.hpp"

extern "C" {
#include "../src/inc.h"
#include "../src/opcodes.h"
#include "../src/registers.h"
#include "../src/types.h"
}

static GB_UBYTE* GetStoreRegINC_R8(gameboy* const, GB_UBYTE);
static GB_UWORD* GetStoreRegINC_R16(gameboy* const, GB_UBYTE);

static GB_UBYTE DecodeAndExecuteINC_R8(gameboy* const, GB_UBYTE);
static GB_UBYTE DecodeAndExecuteINC_R16(gameboy* const, GB_UBYTE);

static std::vector<GB_UBYTE> INC_R8_opcodes = std::vector<GB_UBYTE>{
	0x04, // inc a
	0x14, // inc b
	0x24, // inc c
	0x0C, // inc d
	0x1C, // inc e
	0x2C, // inc h
	0x3C, // inc l
};

static std::vector<GB_UBYTE> INC_R16_opcodes = std::vector<GB_UBYTE>{
	0x03, // inc bc
	0x13, // inc de
	0x23, // inc hl
	0x33, // inc sp
};

TEST(INC_R8, ZeroFlag)
{
	InstructionLoop(
		INC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			GB_UBYTE* store = GetStoreRegINC_R8(gb, opcode);
			*store = std::numeric_limits<GB_UBYTE>::max();
			DecodeAndExecuteINC_R8(gb, opcode);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) {
			EXPECT_EQ(flags, ZeroMask());
		});
}

TEST(INC_R8, HalfCarryFlag)
{
	InstructionLoop(
		INC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			GB_UBYTE* store = GetStoreRegINC_R8(gb, opcode);
			*store = 0b1111;
			DecodeAndExecuteINC_R8(gb, opcode);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) {
			EXPECT_EQ(flags, HalfCarryMask());
		});
}

TEST(INC_R8, AllValues)
{
	InstructionLoop(
		INC_R8_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			GB_UBYTE const upper = std::numeric_limits<GB_UBYTE>::max();
			for (GB_UBYTE i = 0; i < upper; ++i) {
				ResetGameboy(gb);
				*GetStoreRegINC_R8(gb, opcode) = i;
				DecodeAndExecuteINC_R8(gb, opcode);
			}
		},
		NoopFlagCheck);
}

TEST(INC_R16, ZeroFlag)
{
	InstructionLoop(
		INC_R16_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			GB_UWORD* store = GetStoreRegINC_R16(gb, opcode);
			*store = std::numeric_limits<GB_UWORD>::max();
			DecodeAndExecuteINC_R16(gb, opcode);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) {
			EXPECT_EQ(flags, ZeroMask());
		});
}

TEST(INC_R16, HalfCarryFlag)
{
	InstructionLoop(
		INC_R16_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			GB_UWORD* store = GetStoreRegINC_R16(gb, opcode);
			*store = 0b1111;
			DecodeAndExecuteINC_R16(gb, opcode);
		},
		[](GB_UBYTE const flags, GB_UBYTE const) {
			EXPECT_EQ(flags, HalfCarryMask());
		});
}

TEST(INC_R16, AllValues)
{
	InstructionLoop(
		INC_R16_opcodes,
		[](gameboy* const gb, GB_UBYTE const opcode) {
			const GB_UWORD upper = std::numeric_limits<GB_UWORD>::max();
			for (GB_UWORD i = 0; i < upper; ++i) {
				ResetGameboy(gb);

				GB_UWORD* store = GetStoreRegINC_R16(gb, opcode);
				*store = i;
				DecodeAndExecuteINC_R16(gb, opcode);
			}
		},
		NoopFlagCheck);
}

static GB_UBYTE* GetStoreRegINC_R8(gameboy* const gb, GB_UBYTE opcode)
{
	GB_UBYTE* store = nullptr;
	switch (opcode) {
		case 0x3C: {
			store = A(gb);
			break;
		}
		case 0x04: {
			store = B(gb);
			break;
		}
		case 0x0C: {
			store = C(gb);
			break;
		}
		case 0x14: {
			store = D(gb);
			break;
		}
		case 0x1C: {
			store = E(gb);
			break;
		}
		case 0x24: {
			store = H(gb);
			break;
		}
		case 0x2C: {
			store = L(gb);
			break;
		}
	}

	EXPECT_NE(store, nullptr);
	return store;
}

static GB_UWORD* GetStoreRegINC_R16(gameboy* const gb, GB_UBYTE opcode)
{
	GB_UWORD* store = nullptr;
	switch (opcode) {
		case 0x03: {
			store = BC(gb);
			break;
		}
		case 0x13: {
			store = DE(gb);
			break;
		}
		case 0x23: {
			store = HL(gb);
			break;
		}
		case 0x33: {
			store = SP(gb);
			break;
		}
	}

	EXPECT_NE(store, nullptr);
	return store;
}

static GB_UBYTE DecodeAndExecuteINC_R8(gameboy* const gb, GB_UBYTE opcode)
{
	GB_UBYTE* store = GetStoreRegINC_R8(gb, opcode);

	GB_UBYTE old = *store;
	DecodeAndExecuteInstruction(gb, opcode);

	EXPECT_EQ(static_cast<GB_UBYTE>(old + 1), *store);
	return *F(gb);
}

static GB_UBYTE DecodeAndExecuteINC_R16(gameboy* const gb, GB_UBYTE opcode)
{
	GB_UWORD* store = GetStoreRegINC_R16(gb, opcode);

	GB_UWORD old = *store;
	DecodeAndExecuteInstruction(gb, opcode);

	EXPECT_EQ(static_cast<GB_UWORD>(old + 1), *store);
	return *F(gb);
}
