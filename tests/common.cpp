#include "common.hpp"

#include <cstring>

void ResetGameboy(gameboy* const gb)
{
	memset(gb, 0, sizeof(*gb));
}

void SetValueInR16(GB_UWORD* r16, GB_UWORD value)
{
	*r16 = value;
}

void SetValueInR8(GB_UBYTE* r8, GB_UBYTE value)
{
	*r8 = value;
}

void NoopFlagCheck(GB_UBYTE const, GB_UBYTE const)
{
	return;
}
