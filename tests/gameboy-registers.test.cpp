#include <gtest/gtest.h>
#include <iostream>
#include <limits>

#include "endianness.hpp"

extern "C" {
#include "../src/gameboy.h"
}

static void register_comparison(const cpu_register*, GB_UWORD);

TEST(GameboyRegisters, Sizes)
{
	gameboy gb;

	EXPECT_EQ(sizeof(gb.registers.all.BC), sizeof(GB_UWORD));
	EXPECT_EQ(sizeof(gb.registers.all.BC.nibble.lo),
			  sizeof(gb.registers.single[0]));

	EXPECT_EQ(sizeof(gb.registers.all),
			  sizeof(gb.registers.single) / sizeof(gb.registers.single[0]));
}

TEST(GameboyRegisters, AddressMapping)
{
	gameboy gb;

	auto* all = &gb.registers.all;

	// TODO: this is only little endian

	// check addresses of getter values with indexes and direct accesses
	EXPECT_EQ(&all->BC.nibble.lo, B(&gb));
	EXPECT_EQ(&all->BC.nibble.lo, &gb.registers.single[0]);

	EXPECT_EQ(&all->BC.nibble.hi, C(&gb));
	EXPECT_EQ(&all->BC.nibble.hi, &gb.registers.single[1]);

	EXPECT_EQ(&all->DE.nibble.lo, D(&gb));
	EXPECT_EQ(&all->DE.nibble.lo, &gb.registers.single[2]);

	EXPECT_EQ(&all->DE.nibble.hi, E(&gb));
	EXPECT_EQ(&all->DE.nibble.hi, &gb.registers.single[3]);

	EXPECT_EQ(&all->HL.nibble.lo, H(&gb));
	EXPECT_EQ(&all->HL.nibble.lo, &gb.registers.single[4]);

	EXPECT_EQ(&all->HL.nibble.hi, L(&gb));
	EXPECT_EQ(&all->HL.nibble.hi, &gb.registers.single[5]);

	EXPECT_EQ(&all->FA.nibble.lo, F(&gb));
	EXPECT_EQ(&all->FA.nibble.lo, &gb.registers.single[6]);

	EXPECT_EQ(&all->FA.nibble.hi, A(&gb));
	EXPECT_EQ(&all->FA.nibble.hi, &gb.registers.single[7]);
}

TEST(GameboyRegisters, ReadingAndWritingDirectly)
{
	gameboy gb;

	int reg_count =
		sizeof(gb.registers.single) / sizeof(gb.registers.single[0]);

	// initialise all registers with incremented values
	for (GB_UBYTE i = 0; i < reg_count; ++i) {
		gb.registers.single[i] = i;
	}

	register_comparison(&gb.registers.all.FA, 0x0706);
	register_comparison(&gb.registers.all.HL, 0x0504);
	register_comparison(&gb.registers.all.DE, 0x0302);
	register_comparison(&gb.registers.all.BC, 0x0100);
}

TEST(GameboyRegisters, GetR16FromUpperOpcodeNibble)
{
	gameboy gb;

	// Example 16 bit LD instructions
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x01), &gb.registers.all.BC.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x11), &gb.registers.all.DE.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x21), &gb.registers.all.HL.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x31), &gb.SP.reg);

	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0xC1), &gb.registers.all.BC.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0xD1), &gb.registers.all.DE.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0xE1), &gb.registers.all.HL.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0xF1), &gb.registers.all.FA.reg);

	// Example 16 bit INC instruction
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x03), &gb.registers.all.BC.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x13), &gb.registers.all.DE.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x23), &gb.registers.all.HL.reg);
	EXPECT_EQ(GetR16FromUpperOpcodeNibble(&gb, 0x33), &gb.SP.reg);
}

static void register_comparison(const cpu_register* cpu_reg, GB_UWORD value)
{
	EXPECT_EQ(memcmp(&cpu_reg->reg, &value, sizeof(value)), 0);

	GB_UBYTE lo = (value & 0x00FF);
	GB_UBYTE hi = (value & 0xFF00) >> 8;

	if (!is_big_endian()) {
		EXPECT_EQ(cpu_reg->nibble.lo, lo);
		EXPECT_EQ(cpu_reg->nibble.hi, hi);
	}
	else {
		EXPECT_EQ(cpu_reg->nibble.lo, hi);
		EXPECT_EQ(cpu_reg->nibble.hi, lo);
	}
}
