#pragma once

#include <algorithm>
#include <functional>
#include <vector>

extern "C" {
#include "../src/registers.h"
#include "../src/types.h"
}

void ResetGameboy(gameboy* const);
void SetValueInR16(GB_UWORD* r16, GB_UWORD);
void SetValueInR8(GB_UBYTE* r8, GB_UBYTE);
void NoopFlagCheck(GB_UBYTE const, GB_UBYTE const);

template <typename... Opcodes>
std::vector<GB_UBYTE>
GenerateOpcodesFromRange(GB_UBYTE from, GB_UBYTE to, Opcodes... skip)
{
	std::vector<GB_UBYTE> opcodes{};
	auto skippable = {skip...};

	for (GB_UBYTE opcode = from; opcode <= to; ++opcode) {
		if (std::find(std::begin(skippable), std::end(skippable), opcode) !=
			std::end(skippable)) {
			continue;
		}

		opcodes.push_back(opcode);
	}

	return opcodes;
}

template <typename ForEachOpcode, typename FlagCheck>
void InstructionLoop(std::vector<GB_UBYTE> opcodes,
					 ForEachOpcode feo,
					 FlagCheck fc)
{
	gameboy gb;
	InstructionLoop(&gb, opcodes, feo, fc);
}

template <typename ForEachOpcode, typename FlagCheck>
void InstructionLoop(gameboy* const gb,
					 std::vector<GB_UBYTE> opcodes,
					 ForEachOpcode feo,
					 FlagCheck fc)
{
	for (const auto opcode : opcodes) {
		ResetGameboy(gb);
		feo(gb, opcode);
		fc(*F(gb), opcode);
	}
}
