#pragma once

#include <stdbool.h>

#include "types.h"

void LD_R8(GB_UBYTE* const, GB_UBYTE* const);
