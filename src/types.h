#pragma once

#include <inttypes.h>

// byte = 8 bit (signed and unsigned)
typedef uint8_t GB_UBYTE;
typedef int8_t GB_SBYTE;

// word = 16 bit (signed and unsigned)
typedef uint16_t GB_UWORD;
typedef int16_t GB_SWORD;

// must cover entire address space (0x200000)
typedef uint32_t GB_ADDR_INDEX;

typedef union {
	struct {
		GB_UBYTE lo, hi;
	} nibble;
	GB_UWORD reg;
} cpu_register;

typedef union {
	struct {
		cpu_register BC, DE, HL, FA;
	} all;
	GB_UBYTE single[4 * 2]; // 4x 16 bit registers, i.e. 8x 8 bit registers
	GB_UWORD together[4];
} cpu_registers;

typedef struct {
	GB_UBYTE* memory;

	cpu_registers registers;
	cpu_register SP, PC;
} gameboy;
