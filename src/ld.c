#include "ld.h"

#include <stdio.h>

void LD_R8(GB_UBYTE* const store, GB_UBYTE* const load)
{
	*store = *load;
}
