#pragma once

#include <stdio.h>

#include "types.h"

GB_UBYTE ReadByte(GB_ADDR_INDEX);
void SetByte(GB_ADDR_INDEX, GB_UBYTE);

void LoadFromFilePath(const char*);
