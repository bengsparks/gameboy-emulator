#include "registers.h"

#include "types.h"

GB_UBYTE* H(gameboy* const gb)
{
	return &gb->registers.all.HL.nibble.lo;
}
GB_UBYTE* L(gameboy* const gb)
{
	return &gb->registers.all.HL.nibble.hi;
}
GB_UBYTE* A(gameboy* const gb)
{
	return &gb->registers.all.FA.nibble.hi;
}
GB_UBYTE* F(gameboy* const gb)
{
	return &gb->registers.all.FA.nibble.lo;
}
GB_UBYTE* B(gameboy* const gb)
{
	return &gb->registers.all.BC.nibble.lo;
}
GB_UBYTE* C(gameboy* const gb)
{
	return &gb->registers.all.BC.nibble.hi;
}
GB_UBYTE* D(gameboy* const gb)
{
	return &gb->registers.all.DE.nibble.lo;
}
GB_UBYTE* E(gameboy* const gb)
{
	return &gb->registers.all.DE.nibble.hi;
}

GB_UWORD* HL(gameboy* const gb)
{
	return &gb->registers.all.HL.reg;
}
GB_UWORD* FA(gameboy* const gb)
{
	return &gb->registers.all.FA.reg;
}
GB_UWORD* BC(gameboy* const gb)
{
	return &gb->registers.all.BC.reg;
}
GB_UWORD* DE(gameboy* const gb)
{
	return &gb->registers.all.DE.reg;
}
GB_UWORD* SP(gameboy* const gb)
{
	return &gb->SP.reg;
}
GB_UWORD* PC(gameboy* const gb)
{
	return &gb->PC.reg;
}

GB_UBYTE* GetStoreR8FromOpcode(gameboy* const gb, GB_UBYTE instr)
{
	return &gb->registers.single[instr >> 3 /*0b11*/ & 7 /*0b111*/];
}

GB_UBYTE* GetLoadR8FromOpcode(gameboy* const gb, GB_UBYTE instr)
{
	return &gb->registers.single[instr & 7 /*0b111*/];
}

GB_UWORD* GetR16FromUpperOpcodeNibble(gameboy* const gb, GB_UBYTE instr)
{
	GB_UBYTE lower = (instr >> 4);
	// 0x0Y -> 0x3Y = BC,DE,HL,SP
	// 0xCY -> 0xFY = BC,DE,HL,AF

	// TODO: Remove branching
	// 0x3 = 0b0011
	// 0xF = 0b1000
	if (lower == 0x3 || lower == 0xF) {
		return (GB_UWORD*[]){&gb->SP.reg,
							 &gb->registers.all.FA.reg}[lower >> 3];
	}

	return &gb->registers.together[lower & 3 /*0b0011*/];
}
