#pragma once

#include "types.h"

GB_UBYTE* A(gameboy* const);
GB_UBYTE* B(gameboy* const);
GB_UBYTE* C(gameboy* const);
GB_UBYTE* D(gameboy* const);
GB_UBYTE* E(gameboy* const);
GB_UBYTE* F(gameboy* const);
GB_UBYTE* H(gameboy* const);
GB_UBYTE* L(gameboy* const);

GB_UWORD* FA(gameboy* const);
GB_UWORD* BC(gameboy* const);
GB_UWORD* DE(gameboy* const);
GB_UWORD* HL(gameboy* const);
GB_UWORD* SP(gameboy* const);
GB_UWORD* PC(gameboy* const);

GB_UBYTE* GetStoreR8FromOpcode(gameboy* const, GB_UBYTE);
GB_UBYTE* GetLoadR8FromOpcode(gameboy* const, GB_UBYTE);

GB_UWORD* GetR16FromUpperOpcodeNibble(gameboy* const, GB_UBYTE);
GB_UWORD* GetLoadR16FromOpcode(gameboy* const, GB_UBYTE);
