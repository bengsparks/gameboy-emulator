#pragma once

#include "types.h"

void ADC_R8(gameboy* const, GB_UBYTE* const, GB_UBYTE* const, GB_UBYTE);
void ADC_R16(gameboy* const, GB_UWORD* const, GB_UWORD* const, GB_UBYTE);
