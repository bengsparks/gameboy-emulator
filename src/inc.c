#include "inc.h"

#include "gameboy.h"

/**
 * Increment 8 bit register store
 *
 * Zero: Set if result is 0
 * Subtract: Reset
 * HalfCarry: Set if carry from bit 3
 * Carry: Not affected
 */
void INC_R8(gameboy* const gb, GB_UBYTE* const store)
{
	++(*store);

	GB_UBYTE flags = (*store == 0 ? 1 : 0) * ZeroMask();
	flags &= ~SubtractMask();
	flags |= (*store == 16 /*0b10000*/ ? 1 : 0) * HalfCarryMask();

	*F(gb) = flags;
}

/**
 * Increment 16 bit register store
 *
 * No flags are affected
 * TODO: NO FLAGS???
 */
void INC_R16(gameboy* const gb, GB_UWORD* const store)
{
	++(*store);

	GB_UBYTE flags = (*store == 0 ? 1 : 0) * ZeroMask();
	flags &= ~SubtractMask();
	flags |= (*store == 16 /*0b10000*/ ? 1 : 0) * HalfCarryMask();

	*F(gb) = flags;
}
