#include "sbc.h"

#include "adc.h"
#include "gameboy.h"

/**
 * Subtract the value in r8 (load) plus the carry flag from A (store)
 *
 * Zero: Set if the result is 0
 * Subtract: 1
 * Half-Carry: Set if overflow from bit 3
 * Carry: Set if overflow from bit 7
 */
void SBC_R8(gameboy* const gb, GB_UBYTE* const store, GB_UBYTE* const load, GB_UBYTE hasCarry)
{
	GB_UBYTE flipped = ~(*load) + 1;
	ADC_R8(gb, store, &flipped, hasCarry);

	*F(gb) |= SubtractMask();
	*F(gb) ^= CarryMask() | HalfCarryMask();
}
