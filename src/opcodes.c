#include "opcodes.h"

#include <stdbool.h>
#include <stdio.h>

#include "adc.h"
#include "inc.h"
#include "ld.h"
#include "sbc.h"

static inline bool IsInvalidOpcode(GB_UBYTE opcode)
{
	switch (opcode) {
		case 0xDB:
		case 0xDD:
		case 0xE3:
		case 0xE4:
		case 0xEB:
		case 0xEC:
		case 0xED:
		case 0xF4:
		case 0xFC:
		case 0xFD: {
			return true;
		}
		default: {
			return false;
		}
	}
}

void DecodeAndExecuteInstruction(gameboy* gb, GB_UBYTE opcode)
{
	if (IsInvalidOpcode(opcode)) {
		return;
	}

	switch (opcode) {
		case 0x03: // inc bc
		case 0x13: // inc de
		case 0x23: // inc hl
		case 0x33: // inc sp
		{
			GB_UWORD* store = GetR16FromUpperOpcodeNibble(gb, opcode);
			INC_R16(gb, store);
			break;
		}

		case 0x3c: // inc a
		case 0x04: // inc b
		case 0x0c: // inc c
		case 0x14: // inc d
		case 0x1c: // inc e
		case 0x24: // inc h
		case 0x2c: // inc l
		{
			GB_UBYTE* store = GetStoreR8FromOpcode(gb, opcode);
			INC_R8(gb, store);
			break;
		}

		case 0x40: // ld b,reg
		case 0x41:
		case 0x42:
		case 0x43:
		case 0x44:
		case 0x45:
		case 0x47:
		case 0x48: // ld c, reg
		case 0x49:
		case 0x4a:
		case 0x4b:
		case 0x4c:
		case 0x4d:
		case 0x4f:
		case 0x50: // ld d,reg
		case 0x51:
		case 0x52:
		case 0x53:
		case 0x54:
		case 0x55:
		case 0x57:
		case 0x58: // ld e, reg
		case 0x59:
		case 0x5a:
		case 0x5b:
		case 0x5c:
		case 0x5d:
		case 0x5f:
		case 0x60: // ld h,reg
		case 0x61:
		case 0x62:
		case 0x63:
		case 0x64:
		case 0x65:
		case 0x67:
		case 0x68: // ld l,reg
		case 0x69:
		case 0x6a:
		case 0x6b:
		case 0x6c:
		case 0x6d:
		case 0x6f:
		case 0x78: // ld a, reg
		case 0x79:
		case 0x7a:
		case 0x7b:
		case 0x7c:
		case 0x7d:
		case 0x7f: {
			GB_UBYTE* const store = GetStoreR8FromOpcode(gb, opcode);
			GB_UBYTE* const load = GetLoadR8FromOpcode(gb, opcode);
			LD_R8(store, load);
			break;
		}

		case 0x80: // add a,r8
		case 0x81:
		case 0x82:
		case 0x83:
		case 0x84:
		case 0x85:
		case 0x87:
		case 0x88: // adc a,r8
		case 0x89:
		case 0x8a:
		case 0x8b:
		case 0x8c:
		case 0x8d:
		case 0x8f: {
			// carry must be 0 or 1
			GB_UBYTE carry = ((opcode & 8 /*0b1000*/) >> 3) & HasCarry(gb);
			GB_UBYTE* const load = GetLoadR8FromOpcode(gb, opcode);
			ADC_R8(gb, A(gb), load, carry);
			break;
		}

		case 0x90: // sub r8
		case 0x91:
		case 0x92:
		case 0x93:
		case 0x94:
		case 0x95:
		case 0x97:
		case 0x98: // sbc a,r8
		case 0x99:
		case 0x9a:
		case 0x9b:
		case 0x9c:
		case 0x9d:
		case 0x9f: {
			GB_UBYTE carry = ((opcode & 8 /*0b1000*/) >> 3) & HasCarry(gb);
			GB_UBYTE* const load = GetLoadR8FromOpcode(gb, opcode);
			SBC_R8(gb, A(gb), load, carry);
			break;
		}

		case 0x09: // add hl, bc
		case 0x19: // add hl, de
		case 0x29: // add hl, hl
		case 0x39: // add hl, sp
		{
			GB_UWORD* const load = GetR16FromUpperOpcodeNibble(gb, opcode);

			ADC_R16(gb, HL(gb), load, 0);
			break;
		}

		default:
			printf("%#02x has not been implemented\n", opcode);
			break;
	}
}
