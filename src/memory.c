#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"

static void LoadFromFile(FILE*);

static GB_UBYTE cartridge[0x200000];

GB_UBYTE ReadByte(GB_ADDR_INDEX i)
{
	assert(/*0 <= i &&*/ i <= sizeof(cartridge));
	return cartridge[i];
}

void SetByte(GB_ADDR_INDEX i, GB_UBYTE b)
{
	cartridge[i] = b;
}

void LoadFromFilePath(const char* filepath)
{
	memset(cartridge, 0, sizeof(cartridge));
	FILE* fp = fopen(filepath, "rb");
	LoadFromFile(fp);
	fclose(fp);
}

static void LoadFromFile(FILE* fp)
{
	fread(cartridge, 1, sizeof(cartridge), fp);
}
