#include "gameboy.h"

// masks for CPU flags
/*static struct {
	// bits 0 to 3 are not used

	// set when carry bit from bit 7 is produced in arithmetic instructions.
	// cleared otherwise
	const GB_UBYTE CARRY;

	// set when carry bit from bit 3 is produced in arithmetic instructions.
	// cleared otherwise
	const GB_UBYTE HALF_CARRY;

	// set when the instruction is a subtraction. cleared otherwise
	const GB_UBYTE SUBTRACT;

	// set when the instruction results in a value of 0. cleared otherwise
	const GB_UBYTE ZERO;
} cpuFlags = {
	// .CARRY = 0x10 0b00010000*/
// ,
//.HALF_CARRY = 0x20 0b00100000*/,
//.SUBTRACT = 0x40 0b01000000*/,
//.ZERO = 0x80 0b10000000*/};

GB_UBYTE CarryMask(void)
{
	// return cpuFlags.CARRY;
	return 0x10;
}

GB_UBYTE HalfCarryMask(void)
{
	// return cpuFlags.HALF_CARRY;
	return 0x20;
}

GB_UBYTE SubtractMask(void)
{
	// return cpuFlags.SUBTRACT;
	return 0x40;
}

GB_UBYTE ZeroMask(void)
{
	// return cpuFlags.ZERO;
	return 0x80;
}

GB_UBYTE HasCarry(gameboy* gb)
{
	return (*F(gb) & CarryMask()) >> 4;
}

bool HasHalfCarry(gameboy* gb)
{
	return (*F(gb) & HalfCarryMask()) >> 5;
}

bool HasSubtract(gameboy* gb)
{
	return (*F(gb) & SubtractMask()) >> 6;
}

bool HasZero(gameboy* gb)
{
	return (*F(gb) & ZeroMask()) >> 7;
}

