#include "gameboy.h"

/**
 * Add the value in r8 (load) plus the carry flag to A
 *
 * Zero: Set if the result is 0
 * Subtract: 0
 * Half-Carry: Set if overflow from bit 3
 * Carry: Set if overflow from bit 7
 */
void ADC_R8(gameboy* const gb, GB_UBYTE* const store, GB_UBYTE* const load, GB_UBYTE hasCarry)
{
	GB_UBYTE const load_ = *load;
	GB_UBYTE const old = *store;

	*store += load_ + hasCarry; // hasCarry is either 0 or 1

	GB_UWORD const correctSum = old + load_;
	GB_UWORD const noCarrySum = old ^ load_;

	// A now contains added values, check for carries
	GB_UWORD const bothCarryInfo = correctSum ^ noCarrySum;

	// map carry flags to 0 and 1
	GB_UBYTE const hasNewHalfCarry = (bothCarryInfo & 0x0010) >> 4;
	GB_UBYTE const hasNewCarry = (bothCarryInfo & 0x0100) >> 8;
	GB_UBYTE const hasNewZero = (*store == 0 ? 1 : 0);

	GB_UBYTE flags = 0;

	// Adjust flags accordingly
	flags |= hasNewZero * ZeroMask();
	flags &= ~SubtractMask();
	flags |= hasNewHalfCarry * HalfCarryMask();
	flags |= hasNewCarry * CarryMask();

	*F(gb) = flags;
}


/**
 * Add the value in r16 (load) plus the carry flag to HL or SP (store)
 *
 * Zero: Set if the result is 0
 * Subtract: 0
 * Half-Carry: Set if overflow from bit 11
 * Carry: Set if overflow from bit 15
 */
void ADC_R16(gameboy* const gb, GB_UWORD* const store, GB_UWORD* const load, GB_UBYTE hasCarry)
{
	GB_UBYTE const old = *store;
	GB_UBYTE const load_ = *load;

	*store += load_ + hasCarry; // hasCarry is either 0 or 1

  // TODO: typedef for next largest width? (uint64_t)
	uint64_t const correctSum = old + load_;
	uint64_t const noCarrySum = old ^ load_;

	// store now contains added values, check for carries
	uint64_t const bothCarryInfo = correctSum ^ noCarrySum;

	// map carry flags to 0 and 1
	GB_UBYTE const hasNewHalfCarry = (bothCarryInfo & 0x0010) >> 10;
	GB_UBYTE const hasNewCarry = (bothCarryInfo & 0x0100) >> 14;
	GB_UBYTE const hasNewZero = (*store == 0 ? 1 : 0);

	GB_UBYTE flags = 0;

	// Adjust flags accordingly
	flags |= hasNewZero * ZeroMask();
	flags &= ~SubtractMask();
	flags |= hasNewHalfCarry * HalfCarryMask();
	flags |= hasNewCarry * CarryMask();

	*F(gb) = flags;
}
