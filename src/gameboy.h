#pragma once

#include <stdbool.h>

#include "registers.h"
#include "types.h"

GB_UBYTE HasCarry(gameboy*);
bool HasHalfCarry(gameboy*);
bool HasSubtract(gameboy*);
bool HasZero(gameboy*);

GB_UBYTE CarryMask(void);
GB_UBYTE HalfCarryMask(void);
GB_UBYTE SubtractMask(void);
GB_UBYTE ZeroMask(void);

