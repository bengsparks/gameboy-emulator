#pragma once

#include "types.h"

void INC_R8(gameboy* const, GB_UBYTE*);
void INC_R16(gameboy* const, GB_UWORD*);
