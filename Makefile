.PHONY: all clean

SRCDIR := src
TSTDIR := tests

BINDIR := bin
TSTBINDIR := test-bin

EXEC_OUT := $(addprefix $(BINDIR)/,gameboy)
EXEC_SRCS := $(wildcard $(addprefix $(SRCDIR)/,*.c))

__EXEC_OBJS := $(subst $(SRCDIR),$(BINDIR),$(EXEC_SRCS))
EXEC_OBJS := $(subst .c,.o,$(__EXEC_OBJS))


TEST := $(addprefix $(TSTBINDIR)/,gtest-all)
TEST_SRCS := $(wildcard $(addprefix $(TSTDIR)/,*.cpp))

__TEST_OBJS := $(subst .cpp,.o,$(TEST_SRCS))
TEST_OBJS := $(subst $(TSTDIR),$(TSTBINDIR),$(__TEST_OBJS))


CFLAGS := -D_DEFAULT_SOURCE -Wall -Wextra -Wpedantic -Werror -g3 -O0 -std=c11
CXXFLAGS := -Wall -Wextra -Wpedantic -Werror -g3 -O0 -std=c++17
CXXLDFLAGS := -lpthread

GTEST_DIR := third-party/googletest/googletest
GMOCK_DIR := third-party/googletest/googlemock

all: $(BINDIR) $(EXEC_OUT) $(TSTBINDIR) $(TEST)

$(EXEC_OUT): $(EXEC_OBJS)
	$(CC) -o $@ $^

$(BINDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(BINDIR):
	mkdir -p $(BINDIR)

$(TSTBINDIR):
	mkdir -p $(TSTBINDIR)

$(TEST): $(TEST_OBJS) $(filter-out bin/main.o,$(EXEC_OBJS))
	$(CXX) $(CXXFLAGS) $(CXXLDFLAGS) $(GTEST_DIR)/src/gtest-all.cc -I$(GTEST_DIR)/include/ -I$(GTEST_DIR) -o $(TEST) $^

$(TSTBINDIR)/%.o: $(TSTDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -I$(GTEST_DIR)/include/ -I$(GTEST_DIR) $< -o $@

clean:
	rm -f $(BINDIR)/*.o $(EXEC_OUT) $(TEST) $(TSTBINDIR)/*.o
